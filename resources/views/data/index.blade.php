<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <h1 class="text-center mb-5">Backend Engineer Test</h1>
    <div class="row">
        <!-- Left Container -->
        <div class="col-6 border-right text-center ml-3">
            <!-- <form action="/answer" method="get"> -->
            <h4>Sample Data</h4>
            <div class="input-group">
                <textarea rows="17" style="height:100%;" id="jsondata" class="form-control" aria-label="With textarea" readonly>{{$data}}</textarea>
            </div>
            <button type="button" class="btn btn-primary btn-lg btn-block mt-3 jawab">Show Result</button>
            <!-- </form> -->
        </div>
        <!-- End of Left Container -->
        <div class="col-5">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Soal Test 1
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <span>There are 6 people aboard with the following data: </span>
                            <div class="col-md-4 text-center">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Age</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>John</td>
                                            <td>15</td>
                                        </tr>
                                        <tr>
                                            <td>Peter</td>
                                            <td>12</td>
                                        </tr>
                                        <tr>
                                            <td>Roger</td>
                                            <td>12</td>
                                        </tr>
                                        <tr>
                                            <td>Anne</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>Mary</td>
                                            <td>15</td>
                                        </tr>
                                        <tr>
                                            <td>Nancy</td>
                                            <td>15</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <span>Because the most occurring age in the data is 15, the output should be an array of strings with the
                                people's name, in this case it should be ['John', 'Mary', 'Nancy'].</span>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Soal Tes 2
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="col-md-4 text-center">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Age</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>John</td>
                                            <td>15</td>
                                        </tr>
                                        <tr>
                                            <td>Peter</td>
                                            <td>12</td>
                                        </tr>
                                        <tr>
                                            <td>Roger</td>
                                            <td>12</td>
                                        </tr>
                                        <tr>
                                            <td>Anne</td>
                                            <td>44</td>
                                        </tr>
                                        <tr>
                                            <td>Mary</td>
                                            <td>15</td>
                                        </tr>
                                        <tr>
                                            <td>Nancy</td>
                                            <td>39</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <span>In the case of the above situation where there are more than one groups of age are at the
                                maximum amount, return both groups in a different array so in this case it should return [['Peter',
                                'Roger'], [‘John’, ’Mary’]]. The example input data will be attached along with this document.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <h3 class="text-center">Result Test 1</h3>
                    <div id="jawaban1" class="input-group">
                        <textarea rows="13" style="height:100%;" name="jsondata" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 class="text-center">Result Test 2</h3>
                    <div id="jawaban2" class="input-group">
                        <textarea rows="13" style="height:100%;" name="jsondata" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <!-- Script -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <!-- Answer -->
        <script>
            $('#collapseExample1').on('show.bs.collapse', function() {
                let jsondata = $("#jsondata").val()
                $.ajax({
                    url: '/answer',
                    type: "get",
                    dataType: 'JSON',
                    data: {
                        _token: '{!! csrf_token() !!}',
                        weight: jsondata,
                    },
                    success: function(response) { // What to do if we succeed
                        if (data == "success")
                            alert(response);
                    },
                    error: function(response) {
                        alert('Error' + response);
                    }
                });
            })
        </script>
        <script>
            $(".jawab").click(function() {
                $.ajax({
                    url: '/answer1',
                    method: "GET",
                    success: function(data) {
                        $('#jawaban1').find('.form-control').html(data)
                    },
                    error: function(error) {
                        console.log(error)
                    }
                })

                $.ajax({
                    url: '/answer2',
                    method: "GET",
                    success: function(data) {
                        $('#jawaban2').find('.form-control').html(data)
                    },
                    error: function(error) {
                        console.log(error)
                    }
                })

            });
        </script>
    </div>
</body>

</html>