

<!doctype html>
<html lang="en-US" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title>Kemitraan Dafabet : Bersama Menuju Visi Utama </title>

        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/img/icons/favicon.ico" rel="shortcut icon">
        <link href="https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/img/icons/touch.png" rel="apple-touch-icon-precomposed">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Dafabet Partnership">


        <!-- This site is optimized with the Yoast SEO plugin v12.5.1 - https://yoast.com/wordpress/plugins/seo/ -->
        <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
        <link rel="canonical" href="https://dafabet-partnership.com/id/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Kemitraan Dafabet : Bersama Menuju Visi Utama" />
        <meta property="og:url" content="https://dafabet-partnership.com/id/" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Kemitraan Dafabet : Bersama Menuju Visi Utama" />
        <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://dafabet-partnership.com/id/#website","url":"https://dafabet-partnership.com/id/","name":"","potentialAction":{"@type":"SearchAction","target":"https://dafabet-partnership.com/id/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://dafabet-partnership.com/id/#webpage","url":"https://dafabet-partnership.com/id/","inLanguage":"en-US","name":"Kemitraan Dafabet : Bersama Menuju Visi Utama","isPartOf":{"@id":"https://dafabet-partnership.com/id/#website"},"datePublished":"2018-09-12T11:14:23+00:00","dateModified":"2018-10-19T07:28:14+00:00"}]}</script>
        <!-- / Yoast SEO plugin. -->

        <link rel='dns-prefetch' href='//s.w.org' />
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/", "ext":".png", "svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/", "svgExt":".svg", "source":{"concatemoji":"https:\/\/dafabet-partnership.com\/id\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3"}};
            !function(e, a, t){var r, n, o, i, p = a.createElement("canvas"), s = p.getContext && p.getContext("2d"); function c(e, t){var a = String.fromCharCode; s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0); var r = p.toDataURL(); return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()}function l(e){if (!s || !s.fillText)return!1; switch (s.textBaseline = "top", s.font = "600 32px Arial", e){case"flag":return!c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447])); case"emoji":return!c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340])}return!1}function d(e){var t = a.createElement("script"); t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)}for (i = Array("flag", "emoji"), t.supports = {everything:!0, everythingExceptFlag:!0}, o = 0; o < i.length; o++)t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[i[o]]); t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function(){t.DOMReady = !0}, t.supports.everything || (n = function(){t.readyCallback()}, a.addEventListener?(a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)):(e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function(){"complete" === a.readyState && t.readyCallback()})), (r = t.source || {}).concatemoji?d(r.concatemoji):r.wpemoji && r.twemoji && (d(r.twemoji), d(r.wpemoji)))}(window, document, window._wpemojiSettings);
        </script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel='stylesheet' id='wp-block-library-css'  href='https://dafabet-partnership.com/id/wp-includes/css/dist/block-library/style.min.css?ver=5.3' media='all' />
        <link rel='stylesheet' id='jscrollpane-css-css'  href='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/css/jscrollpane/jquery.jscrollpane.css?ver=5.3' media='all' />
        <link rel='stylesheet' id='bxslider-css-css'  href='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/css/bxslider/jquery.bxslider.min.css?ver=5.3' media='all' />
        <link rel='stylesheet' id='html5blank-child-css'  href='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/style.css?ver=5.3' media='all' />
        <link rel='stylesheet' id='html5blank-child-responsive-css'  href='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/css/responsive.css?ver=5.3' media='all' />
        <link rel='stylesheet' id='normalize-css'  href='https://dafabet-partnership.com/id/wp-content/themes/html5blank/normalize.css?ver=1.0' media='all' />
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank/js/lib/conditionizr-4.3.0.min.js?ver=4.3.0'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank/js/lib/modernizr-2.7.1.min.js?ver=2.7.1'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank/js/scripts.js?ver=1.0.0'></script>
        <link rel='https://api.w.org/' href='https://dafabet-partnership.com/id/wp-json/' />
        <link rel="alternate" type="application/json+oembed" href="https://dafabet-partnership.com/id/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdafabet-partnership.com%2Fid%2F" />
        <link rel="alternate" type="text/xml+oembed" href="https://dafabet-partnership.com/id/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdafabet-partnership.com%2Fid%2F&#038;format=xml" />
        <script>
// conditionizr.com
// configure environment tests
            conditionizr.config({
            assets: 'https://dafabet-partnership.com/id/wp-content/themes/html5blank-child',
                    tests: {}
            });
        </script>

        <!-- Hotjar Tracking Code for http://www.dafabet.com/ -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:121800, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>


    </head>


    <body class="home page-template page-template-page-home page-template-page-home-php page page-id-5" id="id" lang="id">
        <input type="hidden" name="tpl_url_hidden" id="tpl-url-hidden" value="https://dafabet-partnership.com/hf-tpl">
        <input type="hidden" name="tpl_mobile_responsive" id="tpl-mobile-responsive" value="1">

        <header class="tpl-header tpl-show-lang-text tpl-cf tpl-header1">
            <div class="tpl-inner tpl-cf">
                <div class="tpl-logo-wrap">
                    <a href="#" class="tpl-desk tpl-convert" id="tpl-logo" target="_blank" title="Dafabet">
                        <img src="https://dafabet-partnership.com/hf-tpl/images/logo-dafabet.png">
                    </a>
                </div>

                <nav class="tpl-nav tpl-nav-prod">
                    <a href="#" id="tpl-ow-sports" target="_blank" class="tpl-convert">OW Sports        					        				</a>

                    <a href="#" id="tpl-dafa-sports" target="_blank" class="tpl-convert">Dafa Sport        					        				</a>


                    <a href="#" id="tpl-casino" target="_blank" class="tpl-convert">Kasino        					        				</a>


                    <a href="#" id="tpl-live-dealer" target="_blank" class="tpl-convert">Live Dealer        					        				</a>

                    <a href="#" id="tpl-games" target="_blank" class="tpl-convert">Permainan                                                    </a>

                    <a href="#" id="tpl-arcade" target="_blank" class="tpl-convert">Arcade                                                    </a>

                    <a href="#" id="tpl-lottery" target="_blank" class="tpl-convert">Lotere        					        				</a>

                    <a href="#" id="tpl-poker" target="_blank" class="tpl-convert">Poker        					        				</a>


                    <a href="#" id="tpl-virtuals" target="_blank" class="tpl-convert">Virtual        					        				</a>
                </nav>

                <div class="tpl-lang-wrap">
                    <div class="tpl-lang-trigger tpl-cf">
                        <span class="tpl-sprt id"></span>
                        <span class="tpl-txt-lang">Bahasa Indonesia</span>
                        <span class="tpl-sprt tpl-arw-lang"></span>
                    </div>

                    <ul class="tpl-lang-select">
                        <li class="tpl-lang" id="tpl-lang-en">
                            <a href="../en" id="tpl-en">
                                <span class="tpl-sprt en"></span> <span class="tpl-txt-lang">English (Asia)</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-eu">
                            <a href="../eu" id="tpl-eu">
                                <span class="tpl-sprt eu"></span> <span class="tpl-txt-lang">English (Europe)</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-sc">
                            <a href="../sc" id="tpl-sc">
                                <span class="tpl-sprt sc"></span> <span class="tpl-txt-lang">简体中文</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-ch">
                            <a href="../ch" id="tpl-ch">
                                <span class="tpl-sprt ch"></span> <span class="tpl-txt-lang">繁体中文</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-th">
                            <a href="../th" id="tpl-th">
                                <span class="tpl-sprt th"></span> <span class="tpl-txt-lang">ไทย</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-vn">
                            <a href="../vn" id="tpl-vn">
                                <span class="tpl-sprt vn"></span> <span class="tpl-txt-lang">Tiếng Việt</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>


                        <li class="tpl-lang" id="tpl-lang-jp">
                            <a href="../jp" id="tpl-jp">
                                <span class="tpl-sprt jp"></span> <span class="tpl-txt-lang">日本語</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-kr">
                            <a href="../kr" id="tpl-kr">
                                <span class="tpl-sprt kr"></span> <span class="tpl-txt-lang">한국어</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-in">
                            <a href="../in" id="tpl-in">
                                <span class="tpl-sprt in"></span> <span class="tpl-txt-lang">English India</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-gr">
                            <a href="../gr" id="tpl-gr">
                                <span class="tpl-sprt gr"></span> <span class="tpl-txt-lang">ΕΛΛΗΝΙΚΑ</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>

                        <li class="tpl-lang" id="tpl-lang-pl">
                            <a href="../pl" id="tpl-pl">
                                <span class="tpl-sprt pl"></span> <span class="tpl-txt-lang">Polski</span>
                                <span class="tpl-sprt"></span>
                            </a>
                        </li>




                    </ul>
                </div>

            </div>
        </header>
        <!-- wrapper -->
        <div class="wrapper">

            <!-- header -->
            <header class="header clear" role="banner">

                <div class="header-wrapper">
                    <!-- logo -->
                    <div class="logo">
                        <a href="https://www.dafabookmaker.asia/id/sports">
                            <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                            <img src="https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/img/logo.png" alt="Logo" class="logo-img">
                        </a>
                    </div>
                    <!-- /logo -->

                    <div class="mobile-menu">
                        <span class="ham-menu"></span>
                    </div>

                    <!-- nav -->
                    <div class="nav">
                        <ul id="menu-main-menu" class="menu"><li id="nav-menu-item-29" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#home" class="menu-link main-menu-link">Home</a></li>
                            <li id="nav-menu-item-8" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#teams" class="menu-link main-menu-link">TIM</a></li>
                            <li id="nav-menu-item-9" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#brand-ambassadors" class="menu-link main-menu-link">Ambassador</a></li>
                            <li id="nav-menu-item-10" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#snooker" class="menu-link main-menu-link">Snooker</a></li>
                            <li id="nav-menu-item-545" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://dafabet-partnership.com/id/videos/" class="menu-link main-menu-link">Video</a></li>
                            <li id="nav-menu-item-12" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="https://www.dafabookmaker.asia/id/promotions?product=sports" class="menu-link main-menu-link">PROMOSI</a></li>
                        </ul>					</div>
                    <!-- /nav -->

                    <div class="language-selector">

                        <div class="lang-wrap">
                            <div class="lang-trigger">
                                <span id="id" class="flags-sprite current-lang"></span>
                                <span class="tpl-arw-lang"></span>
                            </div>

                            <ul class="lang-select">
                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/" id="en">
                                        <span class="flags-sprite">en</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/eu/" id="eu">
                                        <span class="flags-sprite">eu</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/sc/" id="sc">
                                        <span class="flags-sprite">sc</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/ch/" id="ch">
                                        <span class="flags-sprite">ch</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/th/" id="th">
                                        <span class="flags-sprite">th</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/vn/" id="vn">
                                        <span class="flags-sprite">vn</span>
                                    </a>
                                </li>


                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/jp/" id="jp">
                                        <span class="flags-sprite">jp</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/kr/" id="kr">
                                        <span class="flags-sprite">kr</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/in/" id="in">
                                        <span class="flags-sprite">in</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/gr/" id="gr">
                                        <span class="flags-sprite">gr</span>
                                    </a>
                                </li>

                                <li class="lang">
                                    <a href="https://dafabet-partnership.com/pl/" id="pl">
                                        <span class="flags-sprite">pl</span>
                                    </a>
                                </li>

                            </ul>
                        </div>	                </div>
                </div>

                <!-- mobile-nav -->
                <div class="mobile-nav nav">
                    <ul id="menu-main-menu-1" class="menu"><li id="nav-menu-item-29" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#home" class="menu-link main-menu-link">Home</a></li>
                        <li id="nav-menu-item-8" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#teams" class="menu-link main-menu-link">TIM</a></li>
                        <li id="nav-menu-item-9" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#brand-ambassadors" class="menu-link main-menu-link">Ambassador</a></li>
                        <li id="nav-menu-item-10" class="main-menu-item  menu-item-even menu-item-depth-0 anchor menu-item menu-item-type-custom menu-item-object-custom"><a href="#snooker" class="menu-link main-menu-link">Snooker</a></li>
                        <li id="nav-menu-item-545" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://dafabet-partnership.com/id/videos/" class="menu-link main-menu-link">Video</a></li>
                        <li id="nav-menu-item-12" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="https://www.dafabookmaker.asia/id/promotions?product=sports" class="menu-link main-menu-link">PROMOSI</a></li>
                    </ul>				</div>
                <!-- /mobile-nav -->

            </header>
            <!-- /header -->

            

            <!-- footer -->
            
            
            <footer class="footer" role="contentinfo">

                <!-- copyright -->
                <p class="copyright">
                    <!-- &copy; 2019 Copyright . Powered by					<a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>. -->
                </p>
                <!-- /copyright -->

            </footer>
            <!-- /footer -->

            <input type='hidden' id='home-url' value='https://dafabet-partnership.com/id' />
            <input type='hidden' id='stylesheet-url' value='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child' />

        </div>
        <!-- /wrapper -->


        <div class="fbanner fbanner-left">
            <div class="fbanner-wrapper">
                <div class="fbanner-content">
                    <a href="https://www.dafabookmaker.asia/id/sports-df/sports" id="fbanner-link-visit-dafabet" class="fbanner-desk" target="_blank"><button>
                            KUNJUNGI<br><img src="https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/img/dafabet-logo-small.png"><br>SEKARANG!					</button></a>
                    <a href="https://m.dafabookmaker.asia/id/" id="fbanner-link-visit-dafabet" class="fbanner-mobi" target="_blank"><button>
                            KUNJUNGI<br><img src="https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/img/dafabet-logo-small.png"><br>SEKARANG!					</button></a>
                </div>
                <div class="fbanner-title"><span>TARUHAN!</span></div>
            </div>
        </div>

        <footer class="tpl-footer tpl-show-sponsors">
            <div class="tpl-inner tpl-cf">
                <section class="tpl-partners">
                    <img class="tpl-desktop-sponsor" src="https://dafabet-partnership.com/hf-tpl/images/sponsors/desktop/sponsors-id.png">
                    <img class="tpl-mobile-sponsor" src="https://dafabet-partnership.com/hf-tpl/images/sponsors/mobile/sponsors-mobile-id.png">
                </section>

                <section class="tpl-contacts ">
                    <span class="tpl-toll">
                        INTERNATIONAL<br>
                        TOLL-FREE:<br>
                        +800-7423-2274                 </span>

                    <a href="javascript:void(0);" id="tpl-chat" target="_blank">
                        <img src="https://dafabet-partnership.com/hf-tpl/images/chat-id.png">
                    </a>

                    <a href="#" id="tpl-email">
                        <img src="https://dafabet-partnership.com/hf-tpl/images/email.png">
                    </a>

                    <span class="tpl-social-media">
                        <a href="#" id="tpl-facebook" target="_blank">
                            <img src="https://dafabet-partnership.com/hf-tpl/images/facebook.png">
                        </a><a href="" id="tpl-twitter" target="_blank">
                            <img src="https://dafabet-partnership.com/hf-tpl/images/twitter.png">
                        </a><a href="#" id="tpl-line" target="_blank">
                            <img src="https://dafabet-partnership.com/hf-tpl/images/line.png">
                        </a><a href="" id="tpl-bbm" target="_blank">
                            <img src="https://dafabet-partnership.com/hf-tpl/images/bbm.png">
                        </a>
                        <span>
                        </span>
                    </span>
                </section>
            </div>
            <div class="tpl-copyright">
                <div class="tpl-inner">
                    Hak cipta &#169; 2019 | dafabet | Semua hak dilindungi        </div>
            </div>
        </footer>

        <script src="https://dafabet-partnership.com/hf-tpl/js/tpl-script-min.js?v=1574659318" type="text/javascript"></script>




        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/js/common.js?ver=5.3'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/js/tracking.js?ver=5.3'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/js/jscrollpane/jquery.mousewheel.min.js?ver=5.3'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/js/jscrollpane/jquery.jscrollpane.min.js?ver=5.3'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-content/themes/html5blank-child/js/bxslider/jquery.bxslider.min.js?ver=5.3'></script>
        <script type='text/javascript' src='https://dafabet-partnership.com/id/wp-includes/js/wp-embed.min.js?ver=5.3'></script>

        <!-- GA Code -->
        <script>
            (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
            (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36828255-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!--/ GA Code -->

    </body>
</html>
