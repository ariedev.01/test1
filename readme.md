> ### TEST Backend Engineer

This repo is functionality complete 

# Getting started

## Installation

1. Clone the repository 
- git clone https://gitlab.com/ariedev.01/test1.git

2. Switch to the repo folder
-  cd test1

3. Install Composer Dependencies
- composer install (note : if you have error on PackageManifest.php, please run "composer update")

4. Install NPM Dependencies
- npm install && npm run dev

5. Create a copy of your .env file
- cp .env.example .env

6. Generate an app encryption key
- php artisan key:generate

7. Start the local development server
- php artisan serve

