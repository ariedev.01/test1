<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
  public function Index()
  {
    $data = file_get_contents(public_path('data.json'));
    return view('data.index', ['data' => $data]);
  }

  public function answer1()
  {
    $data1 = json_decode(file_get_contents(public_path('data.json')), TRUE);
    // dd($data1);
    foreach ($data1 as $ya => $item) {
      $arr[$item['age']][$ya] = $item["name"];
    };

    $result = array();
    foreach (max($arr)  as $question1) {
      $result[] = $question1;
    }
    // dd(json_encode($result));


    return view('data.answer1', ['data' => json_encode($result)]);
  }

  public function answer2()
  {
    $data1 = json_decode(file_get_contents(public_path('data.json')), TRUE);
    // dd($data1);
    foreach ($data1 as $ya => $item) {
      $arr[$item['age']][$ya] = $item["name"];
    };

    $result = array();
    foreach ($arr as $key => $value) {
      $data = $arr[$key];
      sort($data);
      $clength = count($data);
      for ($x = 0; $x < $clength; $x++) {
        $result[] = ['age' => $key, "name" => $data[$x]];
      }
    }

    foreach ($result as $key => $item) {
      $arr1[$item['age']][$key] = $item["name"];
    };

    $jos = array_values($arr1);
    $result2 = array();
    foreach ($jos as $js) {
      $result2[] =  array_values($js);
    }
    return view('data.answer1', ['data' => json_encode($result2)]);
  }

  public function test()
  {
    // data source
    $data1 = json_decode(file_get_contents(public_path('data.json')), TRUE);

    // largest number of people by age
    foreach ($data1 as $key => $item) {
      $arr[$item['age']][$key] = $item["name"];
    }
    echo ('1. Find largest number of people by age');
    echo "<br>";
    echo "Answer :";
    echo "<br>";
    foreach (max($arr) as $question1) {
      echo ($question1 . '|');
    }
    echo "<br>";
    echo "<br>";

    // Sort data by alphabet
    echo ('2. Group by age and sort data by alphabeth');
    echo "<br>";
    echo "Answer :";
    echo "<br>";
    foreach ($arr as $key => $value) {
      $data = $arr[$key];
      sort($data);
      $clength = count($data);
      echo ('Age ' . $key . '=>');
      for ($x = 0; $x < $clength; $x++) {
        echo $data[$x] . '|';
      }
      echo "<br>";
    }
  }
}
